import requests
import json
import pprint
import os

IRODS_RESTURLBASE = os.environ.get("IRODS_RESTURLBASE",
            "http://irodsrest/irods-rest/rest/")

rules = {}

rules["ingestFile"] = (r"""ingestFile {
  # as per rodsKeyWdDef.hpp
  if (errorcode(msiDataObjOpen("objPath="++*logfile++"++++openFlags=O_RDWR",*S_FD)) < 0)
  {
    msiDataObjCreate(*logfile, "destRescName=defaultResc++++createMode=O_RDWR", *S_FD);
  }
  msiDataObjLseek(*S_FD, 0, "SEEK_END", *status);
  msiDataObjWrite(*S_FD, "Queueing "++*path++"\n", *outwrite);
  msiDataObjClose(*S_FD, *status);

  *pendingPath = *path++".pending";
  msiDataObjCreate(*pendingPath, "forceFlag=1++++destRescName=defaultResc++++createMode=O_TRUNC", *P_FD);
  msiDataObjClose(*P_FD, *status);

  delay ("<PLUSET>10s</PLUSET>") {
    *options."objPath" = *path;
    *options."forceFlag" = "1";
    *options."destRescName" = "defaultResc";
    if (errorcode(msiCurlGetObj(*url, *options, *written)) < 0) {
      msiDataObjOpen("objPath="++*logfile++"++++openFlags=O_RDWR",*S_FD);
      msiDataObjLseek(*S_FD, 0, "SEEK_END", *status);
      msiDataObjWrite(*S_FD, "ERROR: Wrote "++*written++" into "++*path++"\n", *outwrite);
      msiDataObjClose(*S_FD, *status);
      msiDataObjCreate(*path++".error", "forceFlag=1++++destRescName=defaultResc++++createMode=O_TRUNC", *E_FD);
      msiDataObjWrite(*E_FD, "ERROR: Wrote "++*written++" into "++*path++"\n", *outwrite);
      msiDataObjClose(*E_FD, *status);
    } else {
      msiDataObjOpen("objPath="++*logfile++"++++openFlags=O_RDWR",*S_FD);
      msiDataObjLseek(*S_FD, 0, "SEEK_END", *status);
      msiDataObjWrite(*S_FD, "SUCCESS: Wrote "++*written++" into "++*path++"\n", *outwrite);
      msiDataObjClose(*S_FD, *status);
    }
    msiDataObjUnlink(*pendingPath, *status);
  }
}
INPUT *url="hi",*path="there",*logfile="friend"
OUTPUT *pendingPath
""", ("*url", "*path", "*logfile"))

rules["checkPendingFile"] = (r"""checkPendingFile {
  *result = "PENDING";
  *c = int(*count);
  for(*i = 0; *i < *c; *i = *i + 1) {
    *err = errorcode(msiCheckAccess(*path, "read object", *status));
    if (*err < 0) {
      *result = "DONE";
      break;
    } else if(*status == 0) {
      *result = "DONE";
      break;
    }
    msiSleep(*sleepTime, "0");
  }
}
INPUT *path="",*count="6",*sleepTime="10"
OUTPUT *result
""", ("*path","*count","*sleepTime"))

class IRodsRuleError(Exception):
    def __init__(self, req):
        self.req = req

    def __repr__(self):
        return "Error: %s" % (self.req.status_code)

class IRodsRuleTransaction(object):
    rule_name = None
    rule_text = None
    args = None
    auth = None
    def __init__(self, rule_name, auth, *args):
        """
        This sets up a particular call to a rule.  Note that the *auth* object
        here is likely a tuple of username, userpass.  *args* are the arguments
        for the function, in order -- not named.  *rule_name* is the name of
        the rule to run.
        """
        self.rule_name = rule_name
        rule_text, arg_names = rules[rule_name]
        self.rule_text = rule_text
        self.args = dict(zip(arg_names, args))
        self.auth = auth

    @property
    def transaction_data(self):
        data = {'ruleProcessingType':'INTERNAL',
                'ruleAsOriginalText': self.rule_text,
                'irodsRuleInputParameters': [
                  {'name': arg_name, 'value': arg_value}
                  for arg_name, arg_value in self.args.items()
                 ]
                }
        return data

    def run(self):
        req = requests.post(IRODS_RESTURLBASE + "/rule/",
                    auth = self.auth,
                    data = json.dumps(self.transaction_data),
                    headers = {'Content-Type': 'application/json',
                               'Accept': 'application/json'})
        if req.status_code != requests.codes.ok:
            raise IRodsRuleError(req)
        rv = json.loads(req.text)
        outputs = dict( (v["parameterName"], v["resultObject"])
                        for v in rv["outputParameterResults"])
        return outputs

