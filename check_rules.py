import sys
import time
import pprint
from irods_rules import IRodsRuleTransaction

r = IRodsRuleTransaction("ingestFile", ('rods', 'rods'),
    'http://yt-project.org/data/sizmbhloz-clref04SNth-rs9_a0.9011.tar.gz',
    '/tempZone/home/rods/ferrari_13.art',
    '/tempZone/home/rods/download.log')

rv = r.run()
pprint.pprint(rv)

cpf = IRodsRuleTransaction("checkPendingFile", ('rods', 'rods'),
    rv['*pendingPath'], '3', '2')
res = None
while res != "DONE":
    res = cpf.run()["*result"]
    print "Polling...", rv["*pendingPath"], res
    time.sleep(5)
print "Done."
